#include "tasksketchmanager.h"

TaskSketchManager::TaskSketchManager() :
	QObject(nullptr),
	PluginBase(this),
	m_userTask(),
	m_dataExtention(new DataExtention(this, m_userTask))
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IUserTaskImageDataExtention), m_dataExtention},
		{INTERFACE(IDataExtention), m_dataExtention},
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_userTask}
	});
}

TaskSketchManager::~TaskSketchManager()
{
}

//QAbstractItemModel *TaskSketchManager::GetModel()
//{
//	return taskModel;
//}

//QAbstractItemModel *TaskSketchManager::GetInternalModel()
//{
//	return sketchItemModel;
//}

//void TaskSketchManager::ConvertSketchToTask(int sketchId)
//{
//	QModelIndex modelIndex = sketchItemModel->index(sketchId, 0);
//	QMap<int, QVariant> map = sketchItemModel->itemData(modelIndex);

//	if(!map.contains(Qt::UserRole))
//	{
//		qCritical() << "Can't resolve data model!";
//		return;
//	}

//	//    QMap<QString, QVariant> dataMap = map[Qt::UserRole].toMap();
//	//    QList<QVariant> data = dataMap[coreRelationName].toList();
//	taskModel->insertRows(taskModel->rowCount(), 1);
//	modelIndex = taskModel->index(taskModel->rowCount()-1, 0);
//	//    dataManager->SetActiveRelation(tableName, coreRelationName);
//	taskModel->setItemData(modelIndex, map);
//	//emit ConvertTaskToSketch(map[0].toInt());
//}

void TaskSketchManager::onReferencesSet()
{
	//	m_dataManager->addExtention(INTERFACE(ITaskSketchManager), INTERFACE(ITaskSketchManager),
	//	{
	//		{"sketch",  QMetaType::Type::QByteArray}
	//	},
	//	{
	//		QByteArray()
	//	});
	//	m_dataManager->addExtention(INTERFACE(ISimpleUserTask), INTERFACE(ITaskSketchManager),
	//	{
	//		{"sketch",  QMetaType::Type::QByteArray}
	//	},
	//	{
	//		QByteArray()
	//	});
	//	sketchItemModel = m_dataManager->getModel(INTERFACE(ITaskSketchManager));
}

void TaskSketchManager::onReady()
{
	//	taskModel = m_dataManager->getModel(INTERFACE(ITaskSketchManager));
}
